import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'ionic.review',
  appName: 'review',
  webDir: 'dist',
  plugins: {
    BackgroundRunner: {
      label: 'com.example.background.task',
      src: 'background.js',
      event: 'checkIn',
      repeat: true,
      interval: 2,
      autoStart: false,
    },
    LocalNotifications: {
      smallIcon: "ic_stat_icon_config_sample",
      iconColor: "#488AFF",
      sound: "beep.wav",
    },
  }
};

export default config;
