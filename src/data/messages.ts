export interface Message {
  fromName: string;
  subject: string;
  isRead: boolean;
  id: number;
}

const messages: Message[] = [
  {
    fromName: 'Camera',
    subject: "Access the device's camera to take pictures or record videos.",
    isRead: true,
    id: 0
  },
  {
    fromName: 'Geolocation',
    subject: "Retrieve the device's current position using GPS or other location services.",
    isRead: true,
    id: 1
  },
  {
    fromName: 'File System',
    subject: "Read from and write to the device's filesystem.",
    isRead: true,
    id: 2

  },
  {
    fromName: 'Device',
    subject: "Access information about the device, such as model, operating system, and more.",
    isRead: true,
    id: 3
  },
  {
    fromName: 'Network',
    subject: "Check the device's network status and get detailed network information.",
    isRead: true,
    id: 4
  },
  {
    fromName: 'Storage',
    subject: "Store data locally using key-value pairs.",
    isRead: true,
    id: 5
  },
  {
    fromName: 'Accelerometer',
    subject: "Access the device's accelerometer to get the acceleration along the x, y, and z axes.",
    isRead: true,
    id: 6
  },
  {
    fromName: 'Gyroscope',
    subject: "Access the device's gyroscope to get the rotation rate around the x, y, and z axes.",
    isRead: true,
    id: 7
  },
  {
    fromName: 'Motion',
    subject: "Get combined accelerometer and gyroscope data.",
    isRead: true,
    id: 8
  },
  {
    fromName: 'Haptic',
    subject: "Haptic Feedback",
    isRead: true,
    id: 9
  },
  {
    fromName: "App (Handle App's State Changes)",
    subject: "Handle app state changes, such as when the app is launched, backgrounded, or resumed.",
    isRead: true,
    id: 10
  },
  {
    fromName: "Browser (Open URL)",
    subject: "Open URL",
    isRead: true,
    id: 11
  },
  {
    fromName: "Video",
    subject: "Play Video",
    isRead: false,
    id: 12
  },
  {
    fromName: "Photo Gallery",
    subject: "Pick Photo",
    isRead: true,
    id: 13
  },
  {
    fromName: "Notification Local",
    subject: "Scheduled Notification",
    isRead: false,
    id: 14
  },
  {
    fromName: "Notification Push",
    subject: "Service Based Notification",
    isRead: true,
    id: 15
  },
  {
    fromName: "Clipboard",
    subject: "Read from and write to the system clipboard.",
    isRead: true,
    id: 16
  },
  {
    fromName: "Share",
    subject: "Share content using the system's sharing capabilities.",
    isRead: true,
    id: 17
  },
  {
    fromName: "Background Geolocation",
    subject: "Continue to receive location updates even when the app is running in the background.",
    isRead: true,
    id: 18
  },
  {
    fromName: "Background Fetch",
    subject: "Perform background data fetching operations.",
    isRead: true,
    id: 19
  },
  {
    fromName: "Modal",
    subject: "Present native modal views.",
    isRead: true,
    id: 20
  },
  {
    fromName: "Dialog",
    subject: "Show native alert, confirm, and prompt dialogs.",
    isRead: true,
    id: 21
  },
  {
    fromName: "Status Bar",
    subject: "Change status bar style",
    isRead: true,
    id: 22
  },
  {
    fromName: "Universe",
    subject: "Test Universe Plugin",
    isRead: true,
    id: 23
  },
  {
    fromName: 'Auth',
    subject: "Store auth locally",
    isRead: true,
    id: 24
  },
  {
    fromName: 'Contacts',
    subject: "Get phone book contacts",
    isRead: true,
    id: 25
  },
  {
    fromName: 'BluetoothLe',
    subject: "Scan available bluetooth devices, connect, disconnect",
    isRead: true,
    id: 26
  },
  {
    fromName: 'Track Event',
    subject: "Track Event by key and value",
    isRead: true,
    id: 27
  },
  {
    fromName: 'Payment',
    subject: "Handle transaction and payment",
    isRead: true,
    id: 28
  }
];

export const getMessages = () => messages;

export const getMessage = (id: number) => messages.find(m => m.id === id);
