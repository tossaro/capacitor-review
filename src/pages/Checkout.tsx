import { IonButton, IonContent, IonPage } from "@ionic/react";
import { Payment } from "@t-universe/bridge/plugins/payment";
import { Toast } from "@t-universe/bridge/plugins/toast";

const Checkout: React.FC = () => {
    const commitTransaction = async () => {
        console.log("setAuthData")
        
        var commit = await Payment.commitTransaction({
          callbackUrl: "http://localhost:5173/message/28"
        });
        console.log(commit)
        await Toast.show({
          text: `commit transaction: ${commit}`
        });
    };

    return (
        <IonPage id="checkout-page">
            <IonContent>
                <IonButton onClick={commitTransaction}>commitTransaction</IonButton>
            </IonContent>
        </IonPage>
    );
};

export default Checkout;