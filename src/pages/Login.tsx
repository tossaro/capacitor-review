import { IonButton, IonContent, IonPage } from "@ionic/react";
import { Auth } from "@t-universe/bridge/plugins/auth";
import { Toast } from "@t-universe/bridge/plugins/toast";

const Login: React.FC = () => {
    const setAuthData = async () => {
        console.log("setAuthData")
        let data = JSON.stringify({
            "access_token": "eyJ0eXAiOiJKV1QiLCJraWQiOiJ3VTNpZklJYUxPVUFSZVJCL0ZHNmVNMVAxUU09IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI0OGUzNzE4Zi00NDRhLTQwNDctYjZmYi02MGIzNTg4M2RiZmIiLCJjdHMiOiJPQVVUSDJfU1RBVEVMRVNTX0dSQU5UIiwiYXV0aF9sZXZlbCI6MCwiYXVkaXRUcmFja2luZ0lkIjoiMmMxZmI3MDMtYThlNS00YWJjLTgyNjEtMTJmZTM5MGQyMGI3LTMwNDY2Iiwic3VibmFtZSI6IjQ4ZTM3MThmLTQ0NGEtNDA0Ny1iNmZiLTYwYjM1ODgzZGJmYiIsImlzcyI6Imh0dHBzOi8vYW06NDQzL2FtL29hdXRoMi90c2VsL215dGVsa29tc2VsL2xpdGUiLCJ0b2tlbk5hbWUiOiJhY2Nlc3NfdG9rZW4iLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwiYXV0aEdyYW50SWQiOiJCRVhCU3EwVTlOOXQydzNoRUlmNWtjMkZYYkEiLCJhdWQiOiJlZjdlMjU0MDZhZGZmZmQyNTRlYjYxYmFkNWI1MmM5OCIsIm5iZiI6MTcyNzMzMDQ0NiwiZ3JhbnRfdHlwZSI6InJlZnJlc2hfdG9rZW4iLCJzY29wZSI6WyJpZGVudGlmaWVyIiwicGhvbmUiLCJvcGVuaWQiLCJwcm9maWxlIl0sImF1dGhfdGltZSI6MTcyNzMxOTExMSwicmVhbG0iOiIvdHNlbC9teXRlbGtvbXNlbC9saXRlIiwiZXhwIjoxNzI3MzczNjQ2LCJpYXQiOjE3MjczMzA0NDYsImV4cGlyZXNfaW4iOjQzMjAwLCJqdGkiOiJyM2swSkhyLVlleW9SYjNmMWdPZ3A0QURuMEUifQ.Hxh10EP5Lx3zKdLbN4z9sN8k6Uoz-Mie0jBAktGsRVtqQUPJiWwEpUVH3a-0Tbwmn3o-KW6Ha35YXD0tZ3kaju4cTqBypHxxPI-3KER0YHgmdWQY3eriqql45y0cyFRndG5sZ4uy0C5l5z5yq3y2DlsOfM1OD5X2VnL6WMs-vUpM9dATfGHEjGiXrKjxt06gETPA1-iLP5CUOKyeQxwKQlLrcuMQIoycGDraujtGWMAgb_EkX4gb2QiA92tBANrlf7Wg6ZnYGZrQ3HwWvGlSqHq7-RdcV5axYWwC32_fBGgwKNmvug0J81F0oWP64cwKd8cCE4ZzJz1M2Y6Dn3eCjw",
            "refresh_token": "eyJ0eXAiOiJKV1QiLCJraWQiOiJ3VTNpZklJYUxPVUFSZVJCL0ZHNmVNMVAxUU09IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI0OGUzNzE4Zi00NDRhLTQwNDctYjZmYi02MGIzNTg4M2RiZmIiLCJjdHMiOiJPQVVUSDJfU1RBVEVMRVNTX0dSQU5UIiwiYXV0aF9sZXZlbCI6MCwiYXVkaXRUcmFja2luZ0lkIjoiMmMxZmI3MDMtYThlNS00YWJjLTgyNjEtMTJmZTM5MGQyMGI3LTMwNDY1Iiwic3VibmFtZSI6IjQ4ZTM3MThmLTQ0NGEtNDA0Ny1iNmZiLTYwYjM1ODgzZGJmYiIsImlzcyI6Imh0dHBzOi8vYW06NDQzL2FtL29hdXRoMi90c2VsL215dGVsa29tc2VsL2xpdGUiLCJ0b2tlbk5hbWUiOiJyZWZyZXNoX3Rva2VuIiwidG9rZW5fdHlwZSI6IkJlYXJlciIsImF1dGhHcmFudElkIjoiQkVYQlNxMFU5Tjl0MnczaEVJZjVrYzJGWGJBIiwic2lkIjoiY05TMTY1U2ZqaE0wTXJHdFNlOFRwakhEZ2VJazNZUGJsZHhSbmJpZzNtcz0iLCJhdWQiOiJlZjdlMjU0MDZhZGZmZmQyNTRlYjYxYmFkNWI1MmM5OCIsImFjciI6IjAiLCJuYmYiOjE3MjczMzA0NDYsIm9wcyI6IllXbGp3ZUp5cVJDbXVrMGg4NkVRYmg1R0RGcyIsImdyYW50X3R5cGUiOiJyZWZyZXNoX3Rva2VuIiwic2NvcGUiOlsiaWRlbnRpZmllciIsInBob25lIiwib3BlbmlkIiwicHJvZmlsZSJdLCJhdXRoX3RpbWUiOjE3MjczMTkxMTEsInJlYWxtIjoiL3RzZWwvbXl0ZWxrb21zZWwvbGl0ZSIsImV4cCI6MTcyNzM3MzY0NiwiaWF0IjoxNzI3MzMwNDQ2LCJleHBpcmVzX2luIjo0MzIwMCwianRpIjoiT1NWbHJBTGNYZnlGOUVPMElneWMxeG9nZVlzIn0.JyW9YMoWxZoImJ4viPmi6VnuIGgKMpEokjzs4cZDMt9fAn9G1K6mThiIdG5c3lI0x2HyqRPPfCtrg4Pc27v0Fe2tOEXD_xMKd9BZFOAdaQV8zVxNz-rp3EU9UXP5TNdavrtl9Rw9lQU7tBH22opfzQd48GME8mcUiW3jMSCYryR-a9q4b3TfAXMNUkWqmC-QeMp5XAubJM5yYwhqUSeXAeWxfR4fMYI3xDN0Q21UJXb3izJ6vn6UMJlEz_VsVW_nMowusReIDhC3gnvQWeB6FyrLK8OWO3bo3lk1KVFsklv7DXrFcjLv8NK-pUvJvtHDZb5KaqcoLbH7v9zgdLFWFA",
            "scope": "identifier phone openid profile",
            "id_token": "eyJ0eXAiOiJKV1QiLCJraWQiOiJ3VTNpZklJYUxPVUFSZVJCL0ZHNmVNMVAxUU09IiwiYWxnIjoiUlMyNTYifQ.eyJhdF9oYXNoIjoiRnZWMXlTOS1WWVRmMmJwWTVGRURhUSIsInN1YiI6IjQ4ZTM3MThmLTQ0NGEtNDA0Ny1iNmZiLTYwYjM1ODgzZGJmYiIsImF1ZGl0VHJhY2tpbmdJZCI6IjJjMWZiNzAzLWE4ZTUtNGFiYy04MjYxLTEyZmUzOTBkMjBiNy0zMDQ2NyIsInN1Ym5hbWUiOiI0OGUzNzE4Zi00NDRhLTQwNDctYjZmYi02MGIzNTg4M2RiZmIiLCJpc3MiOiJodHRwczovL2FtOjQ0My9hbS9vYXV0aDIvdHNlbC9teXRlbGtvbXNlbC9saXRlIiwidG9rZW5OYW1lIjoiaWRfdG9rZW4iLCJ1dWlkIjoiNDhlMzcxOGYtNDQ0YS00MDQ3LWI2ZmItNjBiMzU4ODNkYmZiIiwic2lkIjoiY05TMTY1U2ZqaE0wTXJHdFNlOFRwakhEZ2VJazNZUGJsZHhSbmJpZzNtcz0iLCJhdWQiOiJlZjdlMjU0MDZhZGZmZmQyNTRlYjYxYmFkNWI1MmM5OCIsImFjciI6IjAiLCJvcmcuZm9yZ2Vyb2NrLm9wZW5pZGNvbm5lY3Qub3BzIjoiWVdsandlSnlxUkNtdWswaDg2RVFiaDVHREZzIiwiYXpwIjoiZWY3ZTI1NDA2YWRmZmZkMjU0ZWI2MWJhZDViNTJjOTgiLCJhdXRoX3RpbWUiOjE3MjczMTkxMTEsInJlYWxtIjoiL3RzZWwvbXl0ZWxrb21zZWwvbGl0ZSIsImV4cCI6MTcyNzQxNjg0NiwidG9rZW5UeXBlIjoiSldUVG9rZW4iLCJpYXQiOjE3MjczMzA0NDZ9.B-spGMiZIna_BQiAG3LVqXQFiAaRHCNDHMGI6Pd8bXWSf2cz7dS6uzGv0YuGI8y3MrXgdbFUoLjo2irXr8r9LD8bOkqeFpmpwb7q-xLC3gIDmMDu9j8_7j36TdW8QfcW4WRC2i2D4b2M4zZLRpG5lXPfIxnaYeVlHMn82OpxlQGfkSQBXdRcs4q9XVGtL_XFUz8NBE7gSqluqXYMGXC6GoiEaO5k2ZA1QaGQnbKMcBFkCdNUFaxgaDiFhDDxL7n2tFYmnge9WZVe20Tty8emuKTitsox_fjzIMA8f8Td8Qj7yzt8C9D1ch02oHNOusX7hDw4Hn1bKmvLSdAMUdLOHA",
            "token_type": "Bearer",
            "expires_in": 43199
        })
        var save = await Auth.saveToken({
          publicKey: 'key1',
          callbackUrl: "http://localhost:5173/message/24",
          data: data
        });
        console.log(save)
        await Toast.show({
          text: `auth data saved: ${data}`
        });
    };

    return (
        <IonPage id="login-page">
            <IonContent>
                <IonButton onClick={setAuthData}>setAuthData</IonButton>
            </IonContent>
        </IonPage>
    );
};

export default Login;