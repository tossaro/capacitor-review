import { useRef, useState } from 'react';
import { Message, getMessage } from '../data/messages';
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonModal,
  IonNote,
  IonPage,
  IonToolbar,
  useIonViewWillEnter,
} from '@ionic/react';
import { options, personCircle } from 'ionicons/icons';
import { useParams } from 'react-router';
import { Camera, CameraResultType } from '@t-universe/bridge/plugins/camera'; //'@capacitor/camera';
import { Geolocation, Position } from '@t-universe/bridge/plugins/geolocation'; //'@capacitor/geolocation';
import { BackgroundRunner } from '@t-universe/bridge/plugins/background-runner'; //'@capacitor/background-runner';
import { Haptics, ImpactStyle } from '@t-universe/bridge/plugins/haptics'; //'@capacitor/haptics';
import { Filesystem, Directory, Encoding } from '@t-universe/bridge/plugins/filesystem'; //'@capacitor/filesystem';
import { Browser } from '@t-universe/bridge/plugins/browser'; //'@capacitor/browser';
import { BatteryInfo, Device, DeviceInfo, LanguageTag } from '@t-universe/bridge/plugins/device'; //'@capacitor/device';
import { ConnectionStatus, Network } from '@t-universe/bridge/plugins/network'; //'@capacitor/network';
import { Share } from '@t-universe/bridge/plugins/share'; //'@capacitor/share';
import { Clipboard } from '@t-universe/bridge/plugins/clipboard'; //'@capacitor/clipboard';
import { Dialog } from '@t-universe/bridge/plugins/dialog'; //'@capacitor/dialog';
import { Toast } from '@t-universe/bridge/plugins/toast'; //'@capacitor/toast';
import { ActionSheet, ActionSheetButtonStyle } from '@t-universe/bridge/plugins/action-sheet'; //'@capacitor/action-sheet';
import { PluginListenerHandle } from '@t-universe/bridge'; //'@capacitor/core';
import { Motion } from '@t-universe/bridge/plugins/motion'; //'@capacitor/motion';
import { App } from '@t-universe/bridge/plugins/app'; //'@capacitor/app';
import { Preferences } from '@t-universe/bridge/plugins/preferences'; //'@capacitor/preferences';
import { StatusBar, Style } from '@t-universe/bridge/plugins/status-bar'; //'@capacitor/status-bar';
import { Contacts } from '@t-universe/bridge/plugins/contacts';
import { Common } from '@t-universe/bridge/plugins/common';
import { Auth } from '@t-universe/bridge/plugins/auth';
import { BleClient } from '@capacitor-community/bluetooth-le';
import { PushNotifications } from '@capacitor/push-notifications';
import './ViewMessage.css';
import { Payment } from '@t-universe/bridge/plugins/payment';

function ViewMessage() {
  const [message, setMessage] = useState<Message>();
  const params = useParams<{ id: string }>();
  const [imageUrl, setImage] = useState<string>();
  const [currentLocation, setCurrentLocation] = useState<Position>();
  const modal = useRef<HTMLIonModalElement>(null);
  let accelHandler: PluginListenerHandle;
  let orientationHandler: PluginListenerHandle;

  useIonViewWillEnter(() => {
    const msg = getMessage(parseInt(params.id, 10));
    setMessage(msg);
  });

  const takePicture = async () => {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    });
    setImage(image.webPath);
  }

  const getCurrentPosition = async () => {
    const coordinates = await Geolocation.getCurrentPosition();
    setCurrentLocation(coordinates);
  };

  const startBgTask = async () => {
    await BackgroundRunner.dispatchEvent({
      label: 'com.capacitor.background.check',
      event: 'fetchTest',
      details: {},
    });
  };
  //const { value } = CapacitorKV.get('CHECKINS');

  const hapticsImpactMedium = async () => {
    await Haptics.impact({ style: ImpactStyle.Medium });
  };
  
  const hapticsImpactLight = async () => {
    await Haptics.impact({ style: ImpactStyle.Light });
  };
  
  const hapticsVibrate = async () => {
    await Haptics.vibrate();
  };
  
  const hapticsSelectionStart = async () => {
    await Haptics.selectionStart();
  };
  
  const hapticsSelectionChanged = async () => {
    await Haptics.selectionChanged();
  };
  
  const hapticsSelectionEnd = async () => {
    await Haptics.selectionEnd();
  };

  const writeSecretFile = async () => {
    await Filesystem.writeFile({
      path: 'secrets/text.txt',
      data: 'This is a test',
      directory: Directory.Documents,
      encoding: Encoding.UTF8,
    });
  };
  
  const readSecretFile = async () => {
    const contents = await Filesystem.readFile({
      path: 'secrets/text.txt',
      directory: Directory.Documents,
      encoding: Encoding.UTF8,
    });
  
    console.log('secrets:', contents);
  };
  
  const deleteSecretFile = async () => {
    await Filesystem.deleteFile({
      path: 'secrets/text.txt',
      directory: Directory.Documents,
    });
  };
  
  const readFilePath = async () => {
    // Here's an example of reading a file with a full file path. Use this to
    // read binary data (base64 encoded) from plugins that return File URIs, such as
    // the Camera.
    const contents = await Filesystem.readFile({
      path: 'file:///var/mobile/Containers/Data/Application/22A433FD-D82D-4989-8BE6-9FC49DEA20BB/Documents/text.txt',
    });
  
    console.log('data:', contents);
  };

  const openCapacitorSite = async () => {
    await Browser.open({ url: 'http://capacitorjs.com/' });
  };

  const [deviceInfo, setDeviceInfo] = useState<DeviceInfo>();
  const [batteryInfo, setBatteryInfo] = useState<BatteryInfo>();
  const [lang, setLang] = useState<LanguageTag>();
  const getDeviceInfo = async () => {
    const info = await Device.getInfo();
    console.log("device:" + JSON.stringify(info));
    setDeviceInfo(info);
  };
  const getBatteryInfo = async () => {
    const info = await Device.getBatteryInfo();
    console.log("battery:", info);
    setBatteryInfo(info);
  };
  const getLang = async () => {
    const lang = await Device.getLanguageTag();
    console.log("language:", lang);
    setLang(lang);
  };

  const [networkInfo, setNetworkInfo] = useState<ConnectionStatus>();
  Network.addListener('networkStatusChange', status => {
    setNetworkInfo(status);
  });
  const getNetworkStatus = async () => {
    const status = await Network.getStatus();
    setNetworkInfo(status);
  };

  const share = async () => {
    await Share.share({
      title: 'See cool stuff',
      text: 'Really awesome thing you need to see right meow',
      url: 'http://ionicframework.com/',
      dialogTitle: 'Share with buddies',
    });
  };
  
  const shareText = async () => {
    await Share.share({
      text: 'Really awesome thing you need to see right meow',
    });
  };
  
  const shareUrl = async () => {
    await Share.share({
      url: 'http://ionicframework.com/',
    });
  };
  
  const shareSingleLocalFile = async () => {
    const photo = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    });
    await Share.share({
      url: photo.path,
    });
  };
  
  const shareMultiLocalFile = async () => {
    const { photos } = await Camera.pickImages({
      quality: 90,
    });
    await Share.share({
      files: photos.map(photo => photo.path!),
    });
  };

  const writeToClipboard = async () => {
    await Clipboard.write({
      string: "Hello World!"
    });
    await Toast.show({
      text: `Hello World! copied`,
    });
  };
  
  const checkClipboard = async () => {
    const { type, value } = await Clipboard.read();
    await Toast.show({
      text: `Got ${type} from clipboard: ${value}`,
    });
  };

  const showAlert = async () => {
    await Dialog.alert({
      title: 'Stop',
      message: 'this is an error',
    });
  };
  
  const showConfirm = async () => {
    const { value } = await Dialog.confirm({
      title: 'Confirm',
      message: `Are you sure you'd like to press the red button?`,
    });
  
    await Toast.show({
      text: `Confirmed: ${value}`,
    });
  };
  
  const showPrompt = async () => {
    const { value, cancelled } = await Dialog.prompt({
      title: 'Hello',
      message: `What's your name?`,
    });
  
    await Toast.show({
      text: `Name: ${value}, Canceled: ${cancelled}`,
    });
  };

  const showActionSheet = async () => {
    const result = await ActionSheet.showActions({
      title: 'Photo Options',
      message: 'Select an option to perform',
      options: [
        {
          title: 'Upload',
        },
        {
          title: 'Share',
        },
        {
          title: 'Remove',
          style: ActionSheetButtonStyle.Destructive,
        },
      ],
    });
  
    await Toast.show({
      text: `Action Sheet result: ${result}`,
    });
  };

  const addAccelListener = async () => {
    try {
      await (DeviceOrientationEvent as any).requestPermission();
    } catch (e) {
      await Toast.show({
        text: `req permission motion error: ${e}`
      });
      return;
    }
  
    accelHandler = await Motion.addListener('accel', async event => {
      await Toast.show({
        text: `Device motion event: ${event}`
      });
    });
    await Toast.show({
      text: `listener accel added`
    });
  };
  const stopAcceleration = async () => {
    if (accelHandler) {
      accelHandler.remove();
      await Toast.show({
        text: `accel listener motion event stoped`
      });
    }
  };
  const addOrientationListener = async () => {
    try {
      await (DeviceOrientationEvent as any).requestPermission();
    } catch (e) {
      await Toast.show({
        text: `req permission motion error: ${e}`
      });
      return;
    }
    orientationHandler = await Motion.addListener('orientation', async event => {
      await Toast.show({
        text: `Device motion event: ${event}`
      });
    });
    await Toast.show({
      text: `listener orientation added`
    });
  };
  const stopOrientation = async () => {
    if (orientationHandler) {
      orientationHandler.remove();
      await Toast.show({
        text: `orientation listener motion event stoped`
      });
    }
  };
  const removeMotionListeners = async () => {
    Motion.removeAllListeners();
    await Toast.show({
      text: `all listener motion event removed`
    });
  };

  const checkAppLaunchUrl = async () => {
    const appLaunchUrl = await App.getLaunchUrl();
    await Toast.show({
      text: `App opened with URL: ${appLaunchUrl?.url}`
    });
  };

  const getAppInfo = async () => {
    const appGetInfo = await App.getInfo();
    console.log(appGetInfo)
    await Toast.show({
      text: `App version: ${appGetInfo.version}`
    });
  };

  const setUserData = async () => {
    let data = JSON.stringify({
      id: 1,
      name: 'Max'
    })
    await Preferences.set({
      key: 'user',
      value: data
    });
    console.log(data)
    await Toast.show({
      text: `user data saved: ${data}`
    });
  };

  const getUserData = async () => {
    const ret = await Preferences.get({ key: 'user' });
    if (ret.value) {
      const user = JSON.parse(ret.value);
      console.log(ret.value)
      await Toast.show({
        text: `user data: ${user}`
      });
    } else {
      console.log("user data not found")
      await Toast.show({
        text: `user data not found`
      });
    }
  };

  const removeUserData = async () => {
    const ret = await Preferences.get({ key: 'user' });
    if (ret.value) {
      Preferences.remove({key: "user"})
      await Toast.show({
        text: `user data ${ret.value} has been removed`
      });
    } else {
      await Toast.show({
        text: `user data not found`
      });
    }
  };

  const setStatusBarStyleDark = async () => {
    await StatusBar.setStyle({ style: Style.Dark });
  };
  
  const setStatusBarStyleLight = async () => {
    await StatusBar.setStyle({ style: Style.Light });
  };
  
  const hideStatusBar = async () => {
    await StatusBar.hide();
  };
  
  const showStatusBar = async () => {
    await StatusBar.show();
  };

  const infoStatusBar = async () => {
    var info = await StatusBar.getInfo();
    console.log(JSON.stringify(info))
  };

  const openIntent = async () => {
    console.log("openIntent");
    var info = await Common.openIntent({url:'app-settings:'});
    console.log(JSON.stringify(info))
  };

  const openInBottomSheet = async () => {
    console.log("openInBottomSheet");
    var info = await Common.openInBottomSheet({url:'https://google.com', dismissable: true});
    console.log(JSON.stringify(info))
  };

  const getAuthData = async () => {
    console.log("getAuthData")
    const ret = await Auth.getToken({ publicKey: 'key1', callbackUrl: "http://localhost:5173/message/24" });
    if (ret.data) {
      const user = JSON.parse(ret.data);
      console.log(ret.data)
      await Toast.show({
        text: `auth data: ${user}`
      });
    } else {
      console.log("auth data not found")
      await Toast.show({
        text: `auth data not found`
      });
    }
  };

  const refreshAuthData = async () => {
    console.log("refreshAuthData")
    const ret = await Auth.refreshToken({ publicKey: 'key1', callbackUrl: "http://localhost:5173/message/24" });
    if (ret.data) {
      const user = JSON.parse(ret.data);
      console.log(ret.data)
      await Toast.show({
        text: `refresh data: ${user}`
      });
    } else {
      console.log("auth data not found")
      await Toast.show({
        text: `auth data not found`
      });
    }
  };

  const logout = async () => {
    console.log("logout")
    await Auth.logout()
  };

  const retrieveListOfContacts = async () => {
    console.log("retrieveListOfContacts")
    const projection = {
      // Specify which fields should be retrieved.
      name: true,
      phones: true,
      postalAddresses: true,
    };
  
    const result = await Contacts.getContacts({
      projection,
    });
    console.log(result)
    await Toast.show({
      text: `contacts data: ${result}`
    });
  }

  const scanDevices = async () => {
    try {
      await BleClient.initialize();
  
      await BleClient.requestLEScan(
        {},
        (result) => {
          console.log('received new scan result', result);
        }
      );
  
      setTimeout(async () => {
        await BleClient.stopLEScan();
        console.log('stopped scanning');
      }, 5000);
    } catch (error) {
      console.error(error);
    }
  }

  const addPushNotifListeners = async () => {
    console.log('addPushNotifListeners')
    await PushNotifications.addListener('registration', token => {
      console.info('Registration token: ', token.value);
    });
  
    await PushNotifications.addListener('registrationError', err => {
      console.error('Registration error: ', err.error);
    });
  
    await PushNotifications.addListener('pushNotificationReceived', notification => {
      console.log('Push notification received: ', JSON.stringify(notification));
    });
  
    await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
      console.log('Push notification action performed', notification.actionId, notification.inputValue);
    });
  }
  
  const registerPushNotifications = async () => {
    console.log('registerPushNotifications')
    let permStatus = await PushNotifications.checkPermissions();
  
    if (permStatus.receive === 'prompt') {
      permStatus = await PushNotifications.requestPermissions();
    }
  
    if (permStatus.receive !== 'granted') {
      throw new Error('User denied permissions!');
    }
  
    await PushNotifications.register();
  }
  
  const getDeliveredPushNotifications = async () => {
    console.log('getDeliveredPushNotifications')
    const notificationList = await PushNotifications.getDeliveredNotifications();
    console.log('delivered notifications', JSON.stringify(notificationList));
  }

  const trackEvent = async () => {
    console.log('trackEvent run')
    const trackEvent = await Common.trackEvent({key: "adjust", value: "1r707n"});
    console.log('trackEvent result:', JSON.stringify(trackEvent));
  }

  const startTransaction = async () => {
    console.log('startTransaction run')
    const startTransaction = await Payment.startTransaction({data: "{}"});
    console.log('startTransaction result:', JSON.stringify(startTransaction));
  }

  const getDataTransaction = async () => {
    console.log('getDataTransaction run')
    const getDataTransaction = await Payment.getDataTransaction();
    console.log('getDataTransaction result:', JSON.stringify(getDataTransaction));
  }

  const clearDataTransaction = async () => {
    console.log('clearDataTransaction run')
    const clearDataTransaction = await Payment.clearDataTransaction();
    console.log('clearDataTransaction result:', JSON.stringify(clearDataTransaction));
  }

  return (
    <IonPage id="view-message-page">
      <IonHeader translucent>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton text="Features" defaultHref="/home"></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        {message ? (
          <>
            <IonItem>
              <IonIcon aria-hidden="true" icon={personCircle} color="primary"></IonIcon>
              <IonLabel className="ion-text-wrap">
                <h2>
                  {message.fromName}
                </h2>
              </IonLabel>
            </IonItem>

            <div className="ion-padding">
              <h1>{message.subject}</h1>
              <br/>
              <center>
                {/* geolocation */}
                { message.id == 1 ? (
                  <>
                    <IonButton onClick={getCurrentPosition}>Get Current Location</IonButton>
                    <br/><br/>
                    <span>Latitude: {currentLocation?.coords.latitude}</span>
                    <br/>
                    <span>Longitude: {currentLocation?.coords.longitude}</span>
                  </>
                ) : <div/> }
                {/* filesystem */}
                { message.id == 2 ? (
                  <>
                    <IonButton onClick={writeSecretFile}>writeSecretFile</IonButton>
                    <br/><br/>
                    <IonButton onClick={readSecretFile}>readSecretFile</IonButton>
                    <br/><br/>
                    <IonButton onClick={deleteSecretFile}>deleteSecretFile</IonButton>
                    <br/><br/>
                    <IonButton onClick={readFilePath}>readFilePath</IonButton>
                  </>
                ) : <div/> }
                {/* device & battery */}
                { message.id == 3 ? (
                  <>
                    <IonButton onClick={getDeviceInfo}>Get Device Info</IonButton>
                    <br/><br/>
                    Device Name: {deviceInfo?.name}
                    <br/>
                    Device Model: {deviceInfo?.model}
                    <br/>
                    Manufacturer: {deviceInfo?.manufacturer}
                    <br/>
                    OS: {deviceInfo?.operatingSystem}
                    <br/>
                    OS Version: {deviceInfo?.osVersion}
                    <br/>
                    Platform: {deviceInfo?.platform}
                    <br/>
                    Memory Used: {deviceInfo?.memUsed}
                    <br/>
                    Disk Free: {deviceInfo?.realDiskFree}
                    <br/><br/>
                    <IonButton onClick={getBatteryInfo}>Get Battery Info</IonButton>
                    <br/><br/>
                    Battery charging: {batteryInfo?.isCharging?.toString()}
                    <br/>
                    Battery level: {batteryInfo?.batteryLevel}<br/><br/>
                    <IonButton onClick={getLang}>Get Language</IonButton>
                    <br/><br/>
                    Language: {lang?.value?.toString()}
                  </>
                ) : <div/> }
                {/* network status */}
                { message.id == 4 ? (
                  <>
                    <IonButton onClick={getNetworkStatus}>Get Network Status</IonButton>
                    <br/><br/>
                    Network Status: {networkInfo?.connected.toString()}
                    <br/>
                    Network Type: {networkInfo?.connectionType}
                  </>
                ) : <div/> }
                {/* storage */}
                { message.id == 5 ? (
                  <>
                    <IonButton onClick={setUserData}>setUserData</IonButton>
                    <br/><br/>
                    <IonButton onClick={getUserData}>getUserData</IonButton>
                    <br/><br/>
                    <IonButton onClick={removeUserData}>removeUserData</IonButton>
                  </>
                ) : <div/> }
                {/* haptics */}
                { message.id == 9 ? (
                  <>
                    <IonButton onClick={hapticsImpactLight}>hapticsImpactLight</IonButton>
                    <br/><br/>
                    <IonButton onClick={hapticsImpactMedium}>hapticsImpactMedium</IonButton>
                    <br/><br/>
                    <IonButton onClick={hapticsVibrate}>hapticsVibrate</IonButton>
                    <br/><br/>
                    <IonButton onClick={hapticsSelectionStart}>hapticsSelectionStart</IonButton>
                    <br/><br/>
                    <IonButton onClick={hapticsSelectionChanged}>hapticsSelectionChanged</IonButton>
                    <br/><br/>
                    <IonButton onClick={hapticsSelectionEnd}>hapticsSelectionEnd</IonButton>
                  </>
                ) : <div/> }
                {/* Accelerometer */}
                { message.id == 6 ? (
                  <>
                    <IonButton onClick={addAccelListener}>addAccelListener</IonButton>
                    <br/><br/>
                    <IonButton onClick={stopAcceleration}>stopAcceleration</IonButton>
                  </>
                ) : <div/> }
                {/* Accelerometer, Gyroscope, Motion */}
                { message.id == 7 ? (
                  <>
                    <IonButton onClick={addOrientationListener}>addOrientationListener</IonButton>
                    <br/><br/>
                    <IonButton onClick={stopOrientation}>stopOrientation</IonButton>
                  </>
                ) : <div/> }
                {/* motion */}
                { message.id == 8 ? (
                  <>
                    <IonButton onClick={removeMotionListeners}>removeMotionListeners</IonButton>
                  </>
                ) : <div/> }
                {/* app */}
                { message.id == 10 ? (
                  <>
                    <IonButton onClick={checkAppLaunchUrl}>checkAppLaunchUrl</IonButton>
                    <br/><br/>
                    <IonButton onClick={getAppInfo}>getAppInfo</IonButton>
                  </>
                ) : <div/> }
                {/* browser */}
                { message.id == 11 ? (
                  <>
                    <IonButton onClick={openCapacitorSite}>Open link on browser</IonButton>
                  </>
                ) : <div/> }
                {/* camera & gallery */}
                { message.id == 0 || message.id == 13 ? (
                  <>
                    <IonButton onClick={takePicture}>Pick a Picture</IonButton>
                    <br/><br/>
                    <img src={imageUrl} alt="gambar"/>
                  </>
                ) : <div/> }
                {/* push-notif */}
                { message.id == 15 ? (
                  <>
                    <IonButton onClick={addPushNotifListeners}>addPushNotifListeners</IonButton>
                    <br/><br/>
                    <IonButton onClick={registerPushNotifications}>registerPushNotifications</IonButton>
                    <br/><br/>
                    <IonButton onClick={getDeliveredPushNotifications}>getDeliveredPushNotifications</IonButton>
                  </>
                ) : <div/> }
                {/* clipboard */}
                { message.id == 16 ? (
                  <>
                    <IonButton onClick={writeToClipboard}>Copy Hello World to Clipboard</IonButton>
                    <br/><br/>
                    <IonButton onClick={checkClipboard}>Check Clipboard</IonButton>
                  </>
                ) : <div/> }
                {/* share */}
                { message.id == 17 ? (
                  <>
                    <IonButton onClick={share}>share</IonButton>
                    <br/><br/>
                    <IonButton onClick={shareText}>shareText</IonButton>
                    <br/><br/>
                    <IonButton onClick={shareUrl}>shareUrl</IonButton>
                    <br/><br/>
                    <IonButton onClick={shareSingleLocalFile}>shareSingleLocalFile</IonButton>
                    <br/><br/>
                    <IonButton onClick={shareMultiLocalFile}>shareMultiLocalFile</IonButton>
                  </>
                ) : <div/> }
                {/* background task */}
                { message.id == 18 || message.id == 19 ? (
                  <>
                    <IonButton onClick={startBgTask}>Start Background Task</IonButton>
                    <br/><br/>
                    
                  </>
                ) : <div/> }
                {/* modal */}
                { message.id == 20 ? (
                  <>
                    <IonButton id="open-modal" expand="block">
                      Open Sheet Modal
                    </IonButton>
                    <IonModal
                      trigger="open-modal"
                      initialBreakpoint={0.25}
                      breakpoints={[0, 0.25, 0.5, 0.75]}
                      handleBehavior="cycle"
                    >
                      <IonContent className="ion-padding">
                        <div className="ion-margin-top">
                          <IonLabel>Click the handle above to advance to the next breakpoint.</IonLabel>
                        </div>
                      </IonContent>
                    </IonModal>
                  </>
                ) : <div/> }
                {/* dialog */}
                { message.id == 21 ? (
                  <>
                    <IonButton onClick={showAlert}>Show Alert</IonButton>
                    <br/><br/>
                    <IonButton onClick={showConfirm}>Show Confirm</IonButton>
                    <br/><br/>
                    <IonButton onClick={showPrompt}>Show Prompt</IonButton>
                    <br/><br/>
                    <IonButton onClick={showActionSheet}>Show Action Sheet</IonButton>
                  </>
                ) : <div/> }
                {/* status-bar */}
                { message.id == 22 ? (
                  <>
                    <IonButton onClick={setStatusBarStyleDark}>Set Status Bar Style Dark</IonButton>
                    <br/><br/>
                    <IonButton onClick={setStatusBarStyleLight}>Set Status Bar Style Light</IonButton>
                    <br/><br/>
                    <IonButton onClick={hideStatusBar}>Hide Status Bar</IonButton>
                    <br/><br/>
                    <IonButton onClick={showStatusBar}>Show Status Bar</IonButton>
                    <br/><br/>
                    <IonButton onClick={infoStatusBar}>Info Status Bar</IonButton>
                  </>
                ) : <div/> }
                {/* universe */}
                { message.id == 23 ? (
                  <>
                    <IonButton onClick={openIntent}>Open Setting via intent</IonButton>
                    <br/><br/>
                    <IonButton onClick={openInBottomSheet}>Open google.com in bottom sheet</IonButton>
                    <br/><br/>
                    <a href="mytelkomsellite:">open mytelkomsellite app</a>
                  </>
                ) : <div/> }
                {/* auth */}
                { message.id == 24 ? (
                  <>
                    <br/><br/>
                    <IonButton onClick={getAuthData}>getAuthData</IonButton>
                    <br/><br/>
                    <IonButton onClick={refreshAuthData}>refreshAuthData</IonButton>
                    <br/><br/>
                    <IonButton onClick={logout}>logout</IonButton>
                  </>
                ) : <div/> }
                {/* contacts */}
                { message.id == 25 ? (
                  <>
                    <br/><br/>
                    <IonButton onClick={retrieveListOfContacts}>retrieveListOfContacts</IonButton>
                  </>
                ) : <div/> }
                {/* bluetooth-le */}
                { message.id == 26 ? (
                  <>
                    <br/><br/>
                    <IonButton onClick={scanDevices}>scanDevices</IonButton>
                  </>
                ) : <div/> }
                {/* bluetooth-le */}
                { message.id == 27 ? (
                  <>
                    <br/><br/>
                    <IonButton onClick={trackEvent}>trackEvent</IonButton>
                  </>
                ) : <div/> }
                {/* payment */}
                { message.id == 28 ? (
                  <>
                    <br/><br/>
                    <IonButton onClick={startTransaction}>startTransaction</IonButton>
                    <br/><br/>
                    <IonButton onClick={getDataTransaction}>getDataTransaction</IonButton>
                    <br/><br/>
                    <IonButton onClick={clearDataTransaction}>clearDataTransaction</IonButton>
                  </>
                ) : <div/> }
              </center>
            </div>
          </>
        ) : (
          <div>Message not found</div>
        )}
      </IonContent>
    </IonPage>
  );
}

export default ViewMessage;
